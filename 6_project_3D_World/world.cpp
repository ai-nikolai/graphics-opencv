/********************************************************
*********************************************************
 @Author: Nikolai Rozanov <Nikolai>
 @Date:   29-08-2016
 @Email:  nikolai.rozanov@gmail.com

 (C) Nikolai Rozanov
*********************************************************
*********************************************************/



#include "base.hpp"
#include "soil.h"
#include "drl.hpp"



int id = 0;

bool change = false;
bool keys[1024];


float scale = 1.0f;


glm::vec3 cameraPos   	= glm::vec3(0.0f, 0.0f,  3.0f);
glm::vec3 cameraFront 	= glm::vec3(0.0f, 0.0f, -1.0f);
glm::vec3 cameraUp    	= glm::vec3(0.0f, 1.0f,  0.0f);
glm::vec3 cameraRight 	= glm::vec3(1.0f, 0.0f,	 0.0f);


GLfloat cameraSpeed = 0.03f;





static void key_callback2(GLFWwindow* window, int key, int scancode, int action, int mods)
{

	if (key == GLFW_KEY_SPACE && action == GLFW_PRESS){
		change = !change;
		id = (id+1)%2;
	}
	if (key == GLFW_KEY_ESCAPE && action == GLFW_PRESS)
		glfwSetWindowShouldClose(window, GL_TRUE);

	if (key >= 0 && key < 1024)
    {
        if (action == GLFW_PRESS)
            keys[key] = true;
        else if (action == GLFW_RELEASE)
            keys[key] = false;
    }
}

void do_actions(){

	//speed
	if (keys[GLFW_KEY_F])
		cameraSpeed += 0.002;
	if (keys[GLFW_KEY_H])
		cameraSpeed -= (0.002>cameraSpeed)?0.002:(cameraSpeed/2.0f);


	// Camera controls
	if (keys[GLFW_KEY_W])
		cameraPos += cameraSpeed * cameraFront;
	if (keys[GLFW_KEY_S])
		cameraPos -= cameraSpeed * cameraFront;
	if (keys[GLFW_KEY_A])
		cameraPos -= cameraSpeed * cameraRight;
	if (keys[GLFW_KEY_D])
		cameraPos += cameraSpeed * cameraRight;
	if (keys[GLFW_KEY_UP])
		cameraPos += cameraSpeed * cameraUp;
	if (keys[GLFW_KEY_DOWN])
		cameraPos -= cameraSpeed * cameraUp;
	//scale
	if (keys[GLFW_KEY_P])
		scale += 0.01;
	if (keys[GLFW_KEY_O])
		scale -= 0.01;


}

/*********************************************************************************************
**********************************************************************************************
**********************************************************************************************
**********************************************************************************************
* main function
**********************************************************************************************
**********************************************************************************************
**********************************************************************************************
**********************************************************************************************/
int main(){


	/*********************************************************************************************
	* Initialising stuff
	**********************************************************************************************/
	glfw_init();

	GLFWwindow * window;
	GLFW_window * window_obj = new GLFW_window(640, 640,"First Window!", &window);

	if (window == *(window_obj -> window)){
		std::cout<<"yeyyy :))!";
	}

	window_obj -> use();
	window_obj -> enable_depth();
	window_obj -> set_BGD(0.2f, 0.3f, 0.3f);
	window_obj -> draw();

	std::cout << window_obj -> depth_enabled << std::endl;

	glew_init();

	shader_program shaderprogram("vertex3d.vsh", "fragment3d.fsh");
	shaderprogram.use();







	/*********************************************************************************************
	**********************************************************************************************
	* DATA:
	**********************************************************************************************
	**********************************************************************************************/

	drl::Parser cube("cube.in");
	drl::Parser floor("floor.in");


























	/*********************************************************************************************
	* Texture
	**********************************************************************************************/
	int tex_height[2], tex_width[2];
	unsigned char * wood_texture = SOIL_load_image("wood.jpg", &(tex_width[0]), &(tex_height[0]), 0, SOIL_LOAD_AUTO);
	unsigned char * brick_texture = SOIL_load_image("brick.jpg",&(tex_width[1]), &(tex_height[1]), 0, SOIL_LOAD_AUTO);

	GLuint textureID[2];
	glGenTextures(2, textureID);

	//first
	glBindTexture(GL_TEXTURE_2D, textureID[0]);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, tex_width[0], tex_height[0], 0, GL_RGB, GL_UNSIGNED_BYTE, wood_texture);
	//
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);

	glBindTexture(GL_TEXTURE_2D, 0);
	//second
	glBindTexture(GL_TEXTURE_2D, textureID[1]);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, tex_width[1], tex_height[1], 0, GL_RGB, GL_UNSIGNED_BYTE, brick_texture);
	//
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);

	glBindTexture(GL_TEXTURE_2D, 0);



	//recommended practice
	SOIL_free_image_data(wood_texture);
	SOIL_free_image_data(brick_texture);

	/*********************************************************************************************
	* OpenGl buffer object init
	**********************************************************************************************/


	GLuint vao[2];
	GLuint vbo[3];



	//generating VBO for vertex data
	glGenBuffers(3, vbo);

	//binding the VAO for attributes
	glGenVertexArrays(2, vao);



	GLuint posAttrib = glGetAttribLocation(shaderprogram.shaderprogram, "position");
	GLuint colAttrib = glGetAttribLocation(shaderprogram.shaderprogram, "color");
	GLuint texAttrib = glGetAttribLocation(shaderprogram.shaderprogram, "tex_coord");

	//glBindTexture(GL_TEXTURE_2D, textureID[0]);

	/*********************************************************************************************
	* OpenGl buffer object specification
	**********************************************************************************************/

	//cube
	glBindVertexArray(vao[0]);

	//copying vertex data
	glBindBuffer(GL_ARRAY_BUFFER,vbo[0]);
	glBufferData(GL_ARRAY_BUFFER, sizeof(float)*cube.elements_read, cube.data, GL_STATIC_DRAW);

	// Position attribute
    glVertexAttribPointer(posAttrib, 3, GL_FLOAT, GL_FALSE, 5 * sizeof(GLfloat), (GLvoid*)0);
    glEnableVertexAttribArray(posAttrib);
    // TexCoord attribute
    glVertexAttribPointer(texAttrib, 2, GL_FLOAT, GL_FALSE, 5 * sizeof(GLfloat), (GLvoid*)(3 * sizeof(GLfloat)));
    glEnableVertexAttribArray(texAttrib);



	glEnableVertexAttribArray(posAttrib);
	glEnableVertexAttribArray(texAttrib);








	//floor
	glBindVertexArray(vao[1]);

	//copying vertex data
	glBindBuffer(GL_ARRAY_BUFFER,vbo[1]);
	glBufferData(GL_ARRAY_BUFFER, sizeof(float)*floor.elements_read, floor.data, GL_STATIC_DRAW);

	// Position attribute
	glVertexAttribPointer(posAttrib, 3, GL_FLOAT, GL_FALSE, 5 * sizeof(GLfloat), (GLvoid*)0);
	glEnableVertexAttribArray(posAttrib);
	// TexCoord attribute
	glVertexAttribPointer(texAttrib, 2, GL_FLOAT, GL_FALSE, 5 * sizeof(GLfloat), (GLvoid*)(3 * sizeof(GLfloat)));
	glEnableVertexAttribArray(texAttrib);



	glEnableVertexAttribArray(posAttrib);
	glEnableVertexAttribArray(texAttrib);


	glBindVertexArray(0);







	/*********************************************************************************************
	* Initialising drawing stuff
	**********************************************************************************************/

	GLuint model_location 		= glGetUniformLocation(shaderprogram.shaderprogram, "model");
	GLuint view_location 		= glGetUniformLocation(shaderprogram.shaderprogram, "view");
	GLuint projection_location 	= glGetUniformLocation(shaderprogram.shaderprogram, "projection");



	glm::mat4 view;
	glm::mat4 projection;


	projection = glm::perspective(45.0f, (float)(window_obj -> width) / (float)(window_obj -> height), 0.1f, 100.0f);

	glUniformMatrix4fv(view_location, 1, GL_FALSE, glm::value_ptr(view));
	glUniformMatrix4fv(projection_location, 1, GL_FALSE, glm::value_ptr(projection));

	/*
	* Key Callback
	*/

    //input output
	glfwSetKeyCallback(*(window_obj->window), key_callback2); //escape only




	/*********************************************************************************************
	**********************************************************************************************
	**********************************************************************************************
	* Main loop
	**********************************************************************************************
	**********************************************************************************************
	**********************************************************************************************/
	glm::mat4 model_template;
	glm::mat4 model(1.0f);

	glUniformMatrix4fv(model_location, 1, GL_FALSE, glm::value_ptr(model));

	while (!glfwWindowShouldClose(*(window_obj->window))){

		glfwPollEvents();
		do_actions();

		float time_v = glfwGetTime();
		std::cout <<"time: "<<time_v << "\n";

		//view
		view = glm::lookAt(cameraPos, cameraPos + cameraFront, cameraUp);
		glUniformMatrix4fv(view_location, 1, GL_FALSE, glm::value_ptr(view));


		//model
		if(!change){
			model = glm::rotate(model_template,time_v, glm::vec3(1.0f, 0.0f, 0.0f));
			glUniformMatrix4fv(model_location, 1, GL_FALSE, glm::value_ptr(model));
		}

		//empty the screen
		window_obj -> set_BGD(0.2f, 0.3f, 0.3f);


		//drawing the elements
		//cube
		model = model * scale;
		model[3][3] = 1.0f;
		glUniformMatrix4fv(model_location, 1, GL_FALSE, glm::value_ptr(model));

		glBindTexture(GL_TEXTURE_2D, textureID[0]);
		glBindVertexArray(vao[0]);
		glDrawArrays(GL_TRIANGLES, 0, 36);


		//floor
		model = model / scale;
		model[3][3] = 1.0f;
		glUniformMatrix4fv(model_location, 1, GL_FALSE, glm::value_ptr(model));

		glBindTexture(GL_TEXTURE_2D, textureID[1]);
		glBindVertexArray(vao[1]);
		glDrawArrays(GL_TRIANGLES, 0, 6);

		glBindVertexArray(0);
		glBindTexture(GL_TEXTURE_2D,0);

		window_obj->draw();


	}








	printf("done\n");

	/*********************************************************************************************
	* Ending all processes
	**********************************************************************************************/


	glDeleteVertexArrays(2, vao);
    glDeleteBuffers(3, vbo);

    delete window_obj;
	glfw_destroy();



	return 0;
}
