/********************************************************
*********************************************************
 @Author: Nikolai Rozanov <Nikolai>
 @Date:   29-08-2016
 @Email:  nikolai.rozanov@gmail.com

 (C) Nikolai Rozanov
*********************************************************
*********************************************************/



#include "base.hpp"
#include "soil.h"
#include "glm.hpp"
#include <gtc/matrix_transform.hpp>
#include <gtc/type_ptr.hpp>




bool change = false;


static void key_callback2(GLFWwindow* window, int key, int scancode, int action, int mods)
{
	if (key == GLFW_KEY_SPACE && action == GLFW_PRESS)
		change = !change;
    if (key == GLFW_KEY_ESCAPE && action == GLFW_PRESS)
        glfwSetWindowShouldClose(window, GL_TRUE);
}


/*********************************************************************************************
**********************************************************************************************
**********************************************************************************************
**********************************************************************************************
* main function
**********************************************************************************************
**********************************************************************************************
**********************************************************************************************
**********************************************************************************************/
int main(){


	/*********************************************************************************************
	* Initialising stuff
	**********************************************************************************************/
	glfw_init();

	GLFWwindow * window;
	GLFW_window * window_obj = new GLFW_window(640, 640,"First Window!", &window);

	if (window == *(window_obj -> window)){
		std::cout<<"yeyyy :))!";
	}


	window_obj -> use();
	window_obj -> set_BG(0.2,0.0,0.2);
	window_obj -> draw();

	glew_init();

	shader_program shaderprogram("vertexshader.vsh", "fragmentshader.fsh");
	shaderprogram.use();







	/*********************************************************************************************
	**********************************************************************************************
	* DATA:
	**********************************************************************************************
	**********************************************************************************************/


	float vertices[] = {
		 0.25f,  0.25f, 0.0f,
		 0.25f, -0.25f, 0.0f,
		-0.25f, -0.25f, 0.0f,
		-0.25f,  0.25f, 0.0f
	};

	float colors[]  = {
		0.5f, 0.5f, 0.5f,
		0.5f, 0.5f, 0.5f,
		1.0f, 0.0f, 1.0f,
		0.0f, 0.0f, 0.5f
	};


	unsigned int indices[] = {
		1,2,0,
		0,3,2
	};

	float tex_vertices[] = {
		1.0f, 1.0f,
		1.0f, 0.0f,
		0.0f, 0.0f,
		0.0f, 1.0f
	};



	/*********************************************************************************************
	* Texture
	**********************************************************************************************/
	int tex_height, tex_width;
	unsigned char * tex_image = SOIL_load_image("wood.jpg", &tex_width, &tex_height, 0, SOIL_LOAD_AUTO);


	GLuint texture;
	glGenTextures(1, &texture);
	glBindTexture(GL_TEXTURE_2D, texture);


	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, tex_width, tex_height, 0, GL_RGB, GL_UNSIGNED_BYTE, tex_image);
	glGenerateMipmap(GL_TEXTURE_2D);


	//some parameters
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);

	//recommended practice
	SOIL_free_image_data(tex_image);


	/*********************************************************************************************
	* OpenGl buffer object init
	**********************************************************************************************/


	GLuint vao;
	GLuint vbo[3];
	GLuint ibo;



	//generating VBO for vertex data
	glGenBuffers(3, vbo);

	//generating ibo for index data
	glGenBuffers(1, &ibo);

	//binding the VAO for attributes
	glGenVertexArrays(1, &vao);



	GLuint posAttrib = glGetAttribLocation(shaderprogram.shaderprogram, "position");
	GLuint colAttrib = glGetAttribLocation(shaderprogram.shaderprogram, "Color");
	GLuint texAttrib = glGetAttribLocation(shaderprogram.shaderprogram, "tex_coord");



	/*********************************************************************************************
	* OpenGl buffer object specification
	**********************************************************************************************/


	glBindVertexArray(vao);

	//copying vertex data
	glBindBuffer(GL_ARRAY_BUFFER,vbo[0]);
	glBufferData(GL_ARRAY_BUFFER, 12*sizeof(float), vertices, GL_STATIC_COPY);
	glVertexAttribPointer(posAttrib, 3, GL_FLOAT, GL_TRUE, 3*sizeof(float), 0); // position position 0 in vertexshader


	glBindBuffer(GL_ARRAY_BUFFER,vbo[1]);
	glBufferData(GL_ARRAY_BUFFER, 12*sizeof(float), colors, GL_STATIC_COPY);
	glVertexAttribPointer(colAttrib, 3, GL_FLOAT, GL_TRUE, 3*sizeof(float), 0); // color position 1 in vertexshader


	glBindBuffer(GL_ARRAY_BUFFER,vbo[2]);
	glBufferData(GL_ARRAY_BUFFER, 8*sizeof(float), tex_vertices, GL_STATIC_COPY);
	glVertexAttribPointer(texAttrib, 2, GL_FLOAT, GL_FALSE, 2*sizeof(float), 0); // tex position 2 in vertexshader

	//copying index data!
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ibo);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, 6*sizeof(indices), indices, GL_STATIC_DRAW);



	glEnableVertexAttribArray(posAttrib);
	glEnableVertexAttribArray(colAttrib);
	glEnableVertexAttribArray(texAttrib);











	/*********************************************************************************************
	* Initialising drawing stuff
	**********************************************************************************************/

	GLuint transform_location = glGetUniformLocation(shaderprogram.shaderprogram, "transform_mat");
/*
	// Initial Matrices
	glm::mat4 transform_mat;
	transform_mat = glm::translate(transform_mat, glm::vec3(0.5f, -0.5f, 0.0f));
	transform_mat = glm::rotate(transform_mat, 90.0f, glm::vec3(0.0, 0.0, 1.0));
	//transform_mat = glm::scale(transform_mat, glm::vec3(0.5, 0.5, 0.5));


	//getting transform location in shader program
	glUniformMatrix4fv(transform_location, 1, GL_FALSE, glm::value_ptr(transform_mat));
*/











	/*
	* Key Callback
	*/

    //input output
	glfwSetKeyCallback(*(window_obj->window), key_callback2); //escape only







	/*********************************************************************************************
	**********************************************************************************************
	**********************************************************************************************
	* Main loop
	**********************************************************************************************
	**********************************************************************************************
	**********************************************************************************************/



	while (!glfwWindowShouldClose(*(window_obj->window))){

		glfwPollEvents();


		float time_v = glfwGetTime();
		std::cout <<"time: "<<time_v << "\n";

		if(change){

			glm::mat4 trans;
			//trans = glm::translate(trans, glm::vec3(0.5f, -0.5f, 0.0f));
			trans = glm::rotate(trans,time_v, glm::vec3(0.0f, 0.0f, 1.0f));

			glUniformMatrix4fv(transform_location, 1, GL_FALSE, glm::value_ptr(trans));
		}


		//empty the screen
		window_obj -> set_BG(0.2,0.0,0.2);

		//drawing the elements
		glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, 0);
		window_obj->draw();


	}





	printf("done\n");







	/*********************************************************************************************
	* Ending all processes
	**********************************************************************************************/


	glDeleteVertexArrays(1, &vao);
    glDeleteBuffers(3, vbo);
    glDeleteBuffers(1, &ibo);

    delete window_obj;
	glfw_destroy();



	return 0;
}
