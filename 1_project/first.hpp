#ifndef FIRST_H
#define FIRST_H

/*********************************************************************************************
* All the include's
**********************************************************************************************/

#include <glew.h>
#include <GLFW/glfw3.h>
#include <stdio.h>
#include <stdlib.h>
#include <OpenGL/gl3.h>
#include <fstream>
#include <time.h>
#include <iostream> 
#include <math.h>

#define GLEW_STATIC


/*********************************************************************************************
* glfw window class
**********************************************************************************************/

class GLFW_window{

public:
	//framebuffer size
	int width, height;

	//window pointer
	GLFWwindow * * window;

	//constructors and destructors
	GLFW_window(int, int, const char *, GLFWwindow * *);
	GLFW_window(const char *, GLFWwindow * *);
	~GLFW_window();


	// making window current openGL context
	void set_BG(float, float, float);
	void use();
	void draw();


};

/*********************************************************************************************
* Shader Program
**********************************************************************************************/


class shader_program{

public:

	// program index
	GLuint shaderprogram;

	//constuctro and destructor
	shader_program(const char *, const char *);
	~shader_program();

	//making active
	void use();



};


/*********************************************************************************************
* BUFFER OBJECTS (VBO, IBO, VAO)
**********************************************************************************************/


//no implementation yet.

#endif




