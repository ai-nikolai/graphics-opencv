
#include "first.hpp"








/*********************************************************************************************
**********************************************************************************************
* glfw init and destroy
**********************************************************************************************
**********************************************************************************************/
void glfw_init(){

	if(!glfwInit()){
		printf("smthg went wrong with Init \n");
	}

	glfwWindowHint(GLFW_SAMPLES, 4); // 4x antialiasing
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3); // We want OpenGL 3.3
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
	glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE); // To make MacOS happy; should not be needed
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE); //We don't want the old OpenGL



}


void glfw_destroy(){

	glfwTerminate();

}

/*********************************************************************************************
**********************************************************************************************
* glew init
**********************************************************************************************
**********************************************************************************************/




void glew_init(){
	glewExperimental = GL_TRUE;
	if (!glewInit()){
		printf("Problem with Glew! \n");
	}
}





/*********************************************************************************************
**********************************************************************************************
**********************************************************************************************
**********************************************************************************************
* glfw_window class
**********************************************************************************************
**********************************************************************************************
**********************************************************************************************
**********************************************************************************************/

GLFW_window::GLFW_window(int width_, int height_, const char * window_name, GLFWwindow * * window_in){


	//making window global
	this -> window = window_in;
	//creating the window
	* window = glfwCreateWindow( width_, height_, window_name, NULL, NULL);
	if(!(*(this->window))){
		printf("smth went wrong with winodwInit");
	}


	//getting the size of the framebuffer
	glfwGetFramebufferSize(*(this -> window), &(this->width), &(this->height));

	printf("This is this experiment: %p, and framebuffer size: %d, %d \n", this, this->width, height);

}

GLFW_window::GLFW_window(const char * window_name, GLFWwindow * * window_in){

	//making window "global"
	this -> window = window_in;

	//creating full screen window
	GLFWmonitor * monitor =  glfwGetPrimaryMonitor();
	const GLFWvidmode* mode = glfwGetVideoMode(monitor);

	glfwWindowHint(GLFW_RED_BITS, mode->redBits);
	glfwWindowHint(GLFW_GREEN_BITS, mode->greenBits);
	glfwWindowHint(GLFW_BLUE_BITS, mode->blueBits);
	glfwWindowHint(GLFW_REFRESH_RATE, mode->refreshRate);

	*window = glfwCreateWindow(mode->width, mode->height, window_name, monitor,NULL);


	//getting the size of the framebuffer
	glfwGetFramebufferSize(*(this -> window), &(this->width), &(this->height));
	printf("This is this experiment: %p, and framebuffer size: %d, %d \n", this, this->width, height);

}

GLFW_window::~GLFW_window(){
	printf("window is being deleted! \n");
}





void GLFW_window::use(){

	glfwMakeContextCurrent(*(this->window));
	glViewport(0, 0, this->width, this->height);

}


void GLFW_window::set_BG(float r, float g, float b){

	glClearColor(r, g, b, 1.0f);
	glClear(GL_COLOR_BUFFER_BIT);

}

void GLFW_window::draw(){

	glfwSwapBuffers(*(this -> window));

}





/*********************************************************************************************
**********************************************************************************************
**********************************************************************************************
**********************************************************************************************
* shader class
**********************************************************************************************
**********************************************************************************************
**********************************************************************************************
**********************************************************************************************/

shader_program::shader_program(const char * vertexshader_path, const char * fragmentshader_path){


	//init for the shaders
	GLuint vertexshader, fragmentshader;

	vertexshader = glCreateShader(GL_VERTEX_SHADER);
	fragmentshader = glCreateShader(GL_FRAGMENT_SHADER);



	/*********************************************************************************************
	* setting up shader program
	**********************************************************************************************/
	const GLchar * vshader;
	const GLchar * fshader;

	GLchar * temp_vshader = (GLchar *)malloc(1024);
	GLchar * temp_fshader = (GLchar *)malloc(1024);

	int i = 0;

	std::ifstream file1;
	file1.open(vertexshader_path, std::ios::in);
	if(!file1) {printf("Error with compiling \n"); exit(0);}


	while (file1.good())
    {
       temp_vshader[i] = (GLchar)file1.get();
       if (!file1.eof())
        i++;
    }

    temp_vshader[i] = 0;  // 0-terminate it at the correct position
    file1.close();

    std::ifstream file2;
	file2.open(fragmentshader_path, std::ios::in);
	if(!file2) {std::cout<<"ERROR!2"; exit(0);}

	i =0;

	while (file2.good())
    {
       temp_fshader[i] = (GLchar)file2.get();
       if (!file2.eof())
        i++;
    }

    temp_fshader[i] = 0;  // 0-terminate it at the correct position
    file2.close();

    vshader = temp_vshader;
    fshader = temp_fshader;

	/*********************************************************************************************
	* Compiling shader program
	**********************************************************************************************/

	glShaderSource(vertexshader, 1, &vshader, NULL);
	glCompileShader(vertexshader);

	GLint status;

	glGetShaderiv(vertexshader, GL_COMPILE_STATUS, &status);

	if (status != GL_TRUE){
		char logBuf[1024];
    	glGetShaderInfoLog( vertexshader, sizeof(logBuf), NULL, (GLchar*)logBuf );
    	std::cout<< logBuf;


		std::cout<< "vertexshader compilation error! \n";
		exit(0);
	}

	glShaderSource(fragmentshader, 1, &fshader, NULL);
	glCompileShader(fragmentshader);



	glGetShaderiv(fragmentshader, GL_COMPILE_STATUS, &status);


	if (status != GL_TRUE){

		char logBuf[1024];
    	glGetShaderInfoLog( fragmentshader, sizeof(logBuf), NULL, (GLchar*)logBuf );
    	std::cout<< logBuf;

		std::cout<< "fragmentshader compilation error \n";
		exit(0);
	}

	/*********************************************************************************************
	* Initialising shader program and deleting stuff
	**********************************************************************************************/

	this -> shaderprogram = glCreateProgram();
	glAttachShader(this -> shaderprogram, vertexshader);
	glAttachShader(this -> shaderprogram, fragmentshader);
	glLinkProgram(this -> shaderprogram);


	glGetShaderiv(fragmentshader, GL_LINK_STATUS, &status);



	if (status != GL_TRUE){

		char logBuf[1024];
    	glGetShaderInfoLog( fragmentshader, sizeof(logBuf), NULL, (GLchar*)logBuf );
    	std::cout<< logBuf;

		std::cout<< "linking error!\n";
		exit(0);
	}



	glDeleteShader(vertexshader);
	glDeleteShader(fragmentshader);
	free(temp_vshader);
	free(temp_fshader);

}





shader_program::~shader_program(){
	printf("shader_program is being deleted \n");
}


void shader_program::use(){

	glUseProgram(shaderprogram);

}





/*********************************************************************************************
**********************************************************************************************
* additional stuff
**********************************************************************************************
**********************************************************************************************/

int change = 0;



/*********************************************************************************************
**********************************************************************************************
* Key reactions
**********************************************************************************************
**********************************************************************************************/


static void key_callback(GLFWwindow* window, int key, int scancode, int action, int mods)
{
	if (key == GLFW_KEY_SPACE && action == GLFW_PRESS)
		change = (change + 1) % 2;
    if (key == GLFW_KEY_ESCAPE && action == GLFW_PRESS)
        glfwSetWindowShouldClose(window, GL_TRUE);
}



/*********************************************************************************************
**********************************************************************************************
* drawing
**********************************************************************************************
**********************************************************************************************/

void offset_calc(float * offset, float offset_delta){
	if(change){
		while( (*(offset+1)) > -0.75 && (*(offset)) <0.01) {
			*(offset)	 += 0;
			*(offset+1)	 += (-1*offset_delta);
		}
	}

}

void offset_calc_circle(float * offset, double * theta){

	if (change){
		(*theta) += 0.015;
		(*offset) = cos(*theta)/2;
		(*(offset+1)) = sin(*theta)/2;
	}
	else{
		(*theta) -= 0.025;
		(*offset) = cos(*theta)/2;
		(*(offset+1)) = sin(*theta)/2;
	}
}



/*********************************************************************************************
**********************************************************************************************
**********************************************************************************************
**********************************************************************************************
* main function
**********************************************************************************************
**********************************************************************************************
**********************************************************************************************
**********************************************************************************************/
int main(){


	/*********************************************************************************************
	* Initialising stuff
	**********************************************************************************************/
	glfw_init();

	GLFWwindow * window;
	//GLFW_window * window_obj = new GLFW_window(640, 320,"First Window!", &window);
	GLFW_window * window_obj = new GLFW_window("Really cool stuff", &window);

	if (window == *(window_obj -> window)){
		std::cout<<"yeyyy :))!";
	}


	window_obj -> use();
	window_obj -> set_BG(0.2,0.0,0.2);
	window_obj -> draw();

	glew_init();

	shader_program shaderprogram("vertexshader.vsh", "fragmentshader.fsh");
	shaderprogram.use();






	/*********************************************************************************************
	**********************************************************************************************
	* DATA:
	**********************************************************************************************
	**********************************************************************************************/
/*
	float vertices[] = {
    	-1.00f,  0.75f, 0.0f, 0.5f, 0.5f, 0.5f,
   		-0.75f,  0.75f, 0.0f, 0.5f, 0.5f, 0.5f,
    	-1.00f,  1.00f, 0.0f, 0.5f, 0.5f, 0.5f,
    	-0.75f,  1.00f, 0.0f, 0.5f, 0.5f, 0.5f,
	};
*/
	float vertices[] = {
    	-0.25f, -0.25f, 0.0f, 0.5f, 0.5f, 0.5f,
   		 0.25f, -0.25f, 0.0f, 0.5f, 0.5f, 0.5f,
    	-0.25f,  0.25f, 0.0f, 0.5f, 0.5f, 0.5f,
    	 0.25f,  0.25f, 0.0f, 0.5f, 0.5f, 0.5f,
	};

	float vertices_test[] = {
		-0.25f, -0.25f, 0.0f,
		 0.25f, -0.25f, 0.0f,
		-0.25f,  0.25f, 0.0f,
		 0.25f,  0.25f, 0.0f
	};

	float colors[]  = {
		0.5f, 0.5f, 0.5f,
		0.5f, 0.5f, 0.5f,
		1.0f, 0.0f, 1.0f,
		0.0f, 0.0f, 0.5f
	};


	unsigned int indices[] = {
		0,1,2,
		2,3,1
	};

	float vertices2[] = {
    	-0.5f, -0.5f, 0.0f, 0.0f, 0.5f, 0.8f,
     	 0.5f, -0.5f, 0.0f, 0.3f, 0.4f, 0.7f,
     	 0.0f,  0.5f, 0.0f, 0.7f, 0.6f, 0.4f
	};


	/*********************************************************************************************
	* OpenGl buffer object init
	**********************************************************************************************/


	GLuint vao[2];
	GLuint vbo[2];
	GLuint ibo[2];



	//generating VBO for vertex data
	glGenBuffers(2, vbo);

	//generating ibo for index data
	glGenBuffers(2,ibo);

	//binding the VAO for attributes
	glGenVertexArrays(2, vao);



	GLuint posAttrib = glGetAttribLocation(shaderprogram.shaderprogram, "position");
	GLuint colAttrib = glGetAttribLocation(shaderprogram.shaderprogram, "color");



	/*********************************************************************************************
	* OpenGl buffer object specification
	**********************************************************************************************/


/*
	glBindVertexArray(vao[0]);

	//copying vertex data
	glBindBuffer(GL_ARRAY_BUFFER,vbo[0]);
	glBufferData(GL_ARRAY_BUFFER, 24*sizeof(float), vertices, GL_STATIC_COPY);

	//copying index data!
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ibo[0]);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, 6*sizeof(indices), indices, GL_STATIC_DRAW);


	//assigning attributes
	glVertexAttribPointer(posAttrib, 3, GL_FLOAT, GL_TRUE, 6*sizeof(float), 0); // position position 0 in vertexshader
	glVertexAttribPointer(colAttrib, 3, GL_FLOAT, GL_TRUE, 6*sizeof(float), (void*)(3*sizeof(float))); // color position 1 in vertexshader

	glEnableVertexAttribArray(posAttrib);
	glEnableVertexAttribArray(colAttrib);

	glBindVertexArray(0);

*/

glBindVertexArray(vao[0]);

//copying vertex data
glBindBuffer(GL_ARRAY_BUFFER,vbo[0]);
glBufferData(GL_ARRAY_BUFFER, 12*sizeof(float), vertices_test, GL_STATIC_COPY);
glVertexAttribPointer(posAttrib, 3, GL_FLOAT, GL_TRUE, 3*sizeof(float), 0); // position position 0 in vertexshader


glBindBuffer(GL_ARRAY_BUFFER,vbo[1]);
glBufferData(GL_ARRAY_BUFFER, 12*sizeof(float), colors, GL_STATIC_COPY);
glVertexAttribPointer(colAttrib, 3, GL_FLOAT, GL_TRUE, 3*sizeof(float), 0); // color position 1 in vertexshader

//copying index data!
glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ibo[0]);
glBufferData(GL_ELEMENT_ARRAY_BUFFER, 6*sizeof(indices), indices, GL_STATIC_DRAW);





glEnableVertexAttribArray(posAttrib);
glEnableVertexAttribArray(colAttrib);


/*
	//vertices2

	glBindVertexArray(vao[1]);

	glBindBuffer(GL_ARRAY_BUFFER,vbo[1]);
	glBufferData(GL_ARRAY_BUFFER, 18*sizeof(float), vertices2, GL_STATIC_COPY);

	glVertexAttribPointer(posAttrib, 3, GL_FLOAT, GL_TRUE, 6*sizeof(float), 0); // position position 0 in vertexshader
	glVertexAttribPointer(colAttrib, 3, GL_FLOAT, GL_TRUE, 6*sizeof(float), (void*)(3*sizeof(float))); // color position 1 in vertexshader

	glEnableVertexAttribArray(posAttrib);
	glEnableVertexAttribArray(colAttrib);

	glBindVertexArray(0);
*/

	/*********************************************************************************************
	* Initialising drawing stuff
	**********************************************************************************************/


	//initilising uniforms
	GLfloat time_v, sine_r, sine_g, sine_b;

	//initial offset
	float offset[] = {
		0.0, 0.0, 0.0
	};

	//initial speed
	float offset_delta = 0.005;

	double theta = 0;


	//uniform locations
    GLint vertexColorLocation = glGetUniformLocation(shaderprogram.shaderprogram, "sin_color");
    GLint offsetLocation = glGetUniformLocation(shaderprogram.shaderprogram, "offset");





    //input output
	glfwSetKeyCallback(*(window_obj->window), key_callback);





	/*********************************************************************************************
	**********************************************************************************************
	**********************************************************************************************
	* Main loop
	**********************************************************************************************
	**********************************************************************************************
	**********************************************************************************************/



	while (!glfwWindowShouldClose(*(window_obj->window))){

		glfwPollEvents();

		time_v = glfwGetTime();
		std::cout <<"time: "<<time_v << "\n";
   		sine_r = (sin(1.5*time_v))/2+0.5;
    	sine_g = (sin(2*time_v))/2+0.5;
    	sine_b = (sin(1*time_v))/2+0.5;

    	offset_calc_circle(offset, &theta);


		glUniform3f(vertexColorLocation, sine_r, sine_g, sine_b);
		glUniform3f(offsetLocation, offset[0], offset[1], offset[2]);



		window_obj -> set_BG(1-sine_r,1-sine_g,1-sine_b);
		glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, 0);
		window_obj->draw();


	}





	printf("done\n");







	/*********************************************************************************************
	* Ending all processes
	**********************************************************************************************/


	glDeleteVertexArrays(2, vao);
    glDeleteBuffers(2, vbo);
    glDeleteBuffers(2, ibo);

    delete window_obj;
	glfw_destroy();



	return 0;
}
