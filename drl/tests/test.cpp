/********************************************************
*********************************************************

TO COMPILE: make test

 @Author: Nikolai Rozanov <Nikolai>
 @Date:   04-09-2016
 @Email:  nikolai.rozanov@gmail.com

 (C) Nikolai Rozanov
*********************************************************
*********************************************************/



#include "drl.hpp"

int main(){
    drl::Parser data("test.drl");
    std::cout << data.data[0] << data.data[1] << data.data[2] <<std::endl;
    std::cout << data.data[3] << data.data[4] << data.data[5] <<std::endl;

    return 0;
}
