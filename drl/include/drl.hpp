/********************************************************
*********************************************************
 @Author: Nikolai Rozanov <Nikolai>
 @Date:   29-08-2016
 @Email:  nikolai.rozanov@gmail.com

 (C) Nikolai Rozanov
*********************************************************
*********************************************************/



#ifndef DRL_H
#define DRL_H

/*********************************************************************************************
* All the include's
**********************************************************************************************/

#include <iostream>
#include <fstream>
#include <string>



/*********************************************************************************************
* Parser Class
**********************************************************************************************/
namespace drl{

	class Parser{
		private:
			void parse_first_line(std::string);
			void parse_data_line(std::string);
		public:
			int lines = 0;
			int elements_per_line = 0;
			int elements_read = 0;
			bool file_open = false;
			char delimiter;

			std::ifstream file;
			float * data;


			//functions
			Parser(const char *);
			~Parser();
	};

}

#endif
