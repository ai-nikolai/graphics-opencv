/********************************************************
*********************************************************
 @Author: Nikolai Rozanov <Nikolai>
 @Date:   29-08-2016
 @Email:  nikolai.rozanov@gmail.com

 (C) Nikolai Rozanov
*********************************************************
*********************************************************/



#include "drl.hpp"





/*********************************************************************************************
* Parser Class
**********************************************************************************************/
namespace drl{

	//parse first line
	void Parser::parse_first_line(std::string line){
		std::string token;
		delimiter = line[0];
		line.erase(0,1);

		size_t pos = line.find(delimiter);
		token = line.substr(0, pos);
		line.erase(0, pos + 1);

		lines = stoi(token);
		elements_per_line = stoi(line);
	}
	//parse data
	void Parser::parse_data_line(std::string line){
		std::string token;
		if(line[0]!='#'){
			for(int i = 0; i<elements_per_line-1;i++){
				size_t pos = line.find(delimiter);
				token = line.substr(0, pos);
				line.erase(0, pos + 1);
				data[elements_read] = stof(token);
				elements_read++;
			}
			data[elements_read] = stof(line);
			elements_read++;
		}
	}

	//CONSTRUCTOR
	Parser::Parser(const char * file_name){
		file.open(file_name);
		if(file.is_open()){

			file_open = true;
			std::string line;
			std::cout<< "File Open: " <<file_name << std::endl;

			if (getline(file, line)){
				parse_first_line(line);
				data = (float *) malloc(lines * elements_per_line * sizeof(float));
				while (	getline(file,line)	&&	(elements_read!=(lines*elements_per_line))	){
					parse_data_line(line);
				}
			}
			else{
				std::cerr <<"ERROR FILE EMPTY" << std::endl;
				exit(1);
			}
			file.close();
		}
		else{
			std::cerr <<"ERROR FAILED TO OPEN" << std::endl;
			exit(1);
		}
	}

	//DESTRUCTOR
	Parser::~Parser(){
		if(file_open && lines*elements_per_line>0){
			free(data);
			std::cout<<"deleting parser" <<std::endl;
		}
	}

}
