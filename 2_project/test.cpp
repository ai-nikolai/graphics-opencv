/********************************************************
*********************************************************
 @Author: Nikolai Rozanov <Nikolai>
 @Date:   29-08-2016
 @Email:  nikolai.rozanov@gmail.com

 (C) Nikolai Rozanov
*********************************************************
*********************************************************/



#include "base.hpp"






bool change = false;


static void key_callback2(GLFWwindow* window, int key, int scancode, int action, int mods)
{
	if (key == GLFW_KEY_SPACE && action == GLFW_PRESS)
		change = !change;
    if (key == GLFW_KEY_ESCAPE && action == GLFW_PRESS)
        glfwSetWindowShouldClose(window, GL_TRUE);
}


/*********************************************************************************************
**********************************************************************************************
**********************************************************************************************
**********************************************************************************************
* main function
**********************************************************************************************
**********************************************************************************************
**********************************************************************************************
**********************************************************************************************/
int main(){


	/*********************************************************************************************
	* Initialising stuff
	**********************************************************************************************/
	glfw_init();

	GLFWwindow * window;
	GLFW_window * window_obj = new GLFW_window(640, 320,"First Window!", &window);
	//GLFW_window * window_obj = new GLFW_window("Really cool stuff", &window);

	if (window == *(window_obj -> window)){
		std::cout<<"yeyyy :))!";
	}


	window_obj -> use();
	window_obj -> set_BG(0.2,0.0,0.2);
	window_obj -> draw();

	glew_init();

	shader_program shaderprogram("vertexshader.vsh", "fragmentshader.fsh");
	shaderprogram.use();






	/*********************************************************************************************
	**********************************************************************************************
	* DATA:
	**********************************************************************************************
	**********************************************************************************************/


	float vertices_test[] = {
		-0.25f, -0.25f, 0.0f,
		 0.25f, -0.25f, 0.0f,
		-0.25f,  0.25f, 0.0f,
		 0.25f,  0.25f, 0.0f
	};

	float colors[]  = {
		0.5f, 0.5f, 0.5f,
		0.5f, 0.5f, 0.5f,
		1.0f, 0.0f, 1.0f,
		0.0f, 0.0f, 0.5f
	};


	unsigned int indices[] = {
		0,1,2,
		2,3,1
	};



	/*********************************************************************************************
	* OpenGl buffer object init
	**********************************************************************************************/


	GLuint vao;
	GLuint vbo[2];
	GLuint ibo;



	//generating VBO for vertex data
	glGenBuffers(2, vbo);

	//generating ibo for index data
	glGenBuffers(1, &ibo);

	//binding the VAO for attributes
	glGenVertexArrays(1, &vao);



	GLuint posAttrib = glGetAttribLocation(shaderprogram.shaderprogram, "position");
	GLuint colAttrib = glGetAttribLocation(shaderprogram.shaderprogram, "color");



	/*********************************************************************************************
	* OpenGl buffer object specification
	**********************************************************************************************/


	glBindVertexArray(vao);

	//copying vertex data
	glBindBuffer(GL_ARRAY_BUFFER,vbo[0]);
	glBufferData(GL_ARRAY_BUFFER, 12*sizeof(float), vertices_test, GL_STATIC_COPY);
	glVertexAttribPointer(posAttrib, 3, GL_FLOAT, GL_TRUE, 3*sizeof(float), 0); // position position 0 in vertexshader


	glBindBuffer(GL_ARRAY_BUFFER,vbo[1]);
	glBufferData(GL_ARRAY_BUFFER, 12*sizeof(float), colors, GL_STATIC_COPY);
	glVertexAttribPointer(colAttrib, 3, GL_FLOAT, GL_TRUE, 3*sizeof(float), 0); // color position 1 in vertexshader

	//copying index data!
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ibo);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, 6*sizeof(indices), indices, GL_STATIC_DRAW);



	glEnableVertexAttribArray(posAttrib);
	glEnableVertexAttribArray(colAttrib);



	/*********************************************************************************************
	* Initialising drawing stuff
	**********************************************************************************************/


	//initilising uniforms
	GLfloat time_v, sine_r, sine_g, sine_b;

	//initial offset
	float offset[] = {
		0.0, 0.0, 0.0
	};

	//initial speed
	float offset_delta = 0.005;

	double theta = 0;


	//uniform locations
    GLint vertexColorLocation = glGetUniformLocation(shaderprogram.shaderprogram, "sin_color");
    GLint offsetLocation = glGetUniformLocation(shaderprogram.shaderprogram, "offset");





    //input output
	glfwSetKeyCallback(*(window_obj->window), key_callback2);





	/*********************************************************************************************
	**********************************************************************************************
	**********************************************************************************************
	* Main loop
	**********************************************************************************************
	**********************************************************************************************
	**********************************************************************************************/



	while (!glfwWindowShouldClose(*(window_obj->window))){

		glfwPollEvents();

		time_v = glfwGetTime();
		std::cout <<"time: "<<time_v << "\n";
   		sine_r = (sin(1.5*time_v))/2+0.5;
    	sine_g = (sin(2*time_v))/2+0.5;
    	sine_b = (sin(1*time_v))/2+0.5;

    	offset_calc_circle(offset, &theta, change);


		glUniform3f(vertexColorLocation, sine_r, sine_g, sine_b);
		glUniform3f(offsetLocation, offset[0], offset[1], offset[2]);



		window_obj -> set_BG(1-sine_r,1-sine_g,1-sine_b);
		glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, 0);
		window_obj->draw();


	}





	printf("done\n");







	/*********************************************************************************************
	* Ending all processes
	**********************************************************************************************/


	glDeleteVertexArrays(1, &vao);
    glDeleteBuffers(2, vbo);
    glDeleteBuffers(1, &ibo);

    delete window_obj;
	glfw_destroy();



	return 0;
}
