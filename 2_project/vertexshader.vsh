#version 330 core

in vec3 position;
in vec3 color;


uniform vec3 offset;
uniform vec3 sin_color;

out vec3 Color;

void main(){

	Color = sin_color+color;
	gl_Position = vec4(position.xyz * 0.3 + offset.xyz, 1.0);

}
