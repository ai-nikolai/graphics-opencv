/********************************************************
*********************************************************
 @Author: Nikolai Rozanov <Nikolai>
 @Date:   29-08-2016
 @Email:  nikolai.rozanov@gmail.com

 (C) Nikolai Rozanov
*********************************************************
*********************************************************/



#ifndef BASE_H
#define BASE_H

/*********************************************************************************************
* All the include's
**********************************************************************************************/

#include <glew.h>
#include <GLFW/glfw3.h>
#include <stdio.h>
#include <stdlib.h>
#include <OpenGL/gl3.h>
#include <fstream>
#include <time.h>
#include <iostream>
#include <math.h>
#include "glm.hpp"
#include <gtc/matrix_transform.hpp>
#include <gtc/type_ptr.hpp>

#define GLEW_STATIC






/*********************************************************************************************
* Other Functions
**********************************************************************************************/

void glfw_init();
void glfw_destroy();
void glew_init();
void key_callback(GLFWwindow*, int, int , int, int);




/*********************************************************************************************
* glfw window class
**********************************************************************************************/

class GLFW_window{

public:
	//framebuffer size
	int width, height;
	bool depth_enabled = false, resizable = true;
	//info about window
	const GLFWvidmode * mode;
	GLFWmonitor * monitor;
	//window pointer
	GLFWwindow * * window;

	//constructors and destructors
	GLFW_window(int, int, const char *, GLFWwindow * *);
	GLFW_window(int, int, const char *, GLFWwindow * *, bool);
	GLFW_window(const char *, GLFWwindow * *);
	~GLFW_window();


	// making window current openGL context
	void update();
	void enable_depth();
	void set_BG(float, float, float);
	void set_BGD(float,float,float);
	void use();
	void draw();


};

/*********************************************************************************************
* Shader Program
**********************************************************************************************/


class shader_program{

public:

	// program index
	GLuint shaderprogram;

	//constuctro and destructor
	shader_program(const char *, const char *);
	~shader_program();

	//making active
	void use();



};


/*********************************************************************************************
* Drawing functions
**********************************************************************************************/
void offset_calc(float * , float, bool);
void offset_calc_circle(float * , double *, bool);



#endif
