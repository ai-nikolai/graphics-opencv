/********************************************************
*********************************************************
 @Author: Nikolai Rozanov <Nikolai>
 @Date:   29-08-2016
 @Email:  nikolai.rozanov@gmail.com

 (C) Nikolai Rozanov
*********************************************************
*********************************************************/



#include "base.hpp"


/*********************************************************************************************
**********************************************************************************************
* glfw init and destroy
**********************************************************************************************
**********************************************************************************************/
void glfw_init(){

	if(!glfwInit()){
		printf("smthg went wrong with Init \n");
	}

	glfwWindowHint(GLFW_SAMPLES, 4); // 4x antialiasing
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3); // We want OpenGL 3.3
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
	glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE); // To make MacOS happy; should not be needed
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE); //We don't want the old OpenGL



}


void glfw_destroy(){

	glfwTerminate();

}

/*********************************************************************************************
**********************************************************************************************
* glew init
**********************************************************************************************
**********************************************************************************************/




void glew_init(){
	glewExperimental = GL_TRUE;
	if (!glewInit()){
		printf("Problem with Glew! \n");
	}
}





/*********************************************************************************************
**********************************************************************************************
**********************************************************************************************
**********************************************************************************************
* glfw_window class
**********************************************************************************************
**********************************************************************************************
**********************************************************************************************
**********************************************************************************************/

GLFW_window::GLFW_window(int width_, int height_, const char * window_name, GLFWwindow * * window_in, bool resize_able){
		//
		resizable = resize_able;

		//making window global
		this -> window = window_in;
		//creating the window
		monitor =  glfwGetPrimaryMonitor();
		mode = glfwGetVideoMode(monitor);

		* window = glfwCreateWindow( width_, height_, window_name, NULL, NULL);
		if(!(*(this->window))){
			printf("smth went wrong with winodwInit");
		}
		//
		glfwWindowHint(GLFW_RESIZABLE, resizable);
		//getting the size of the framebuffer
		glfwGetFramebufferSize(*(this -> window), &(this->width), &(this->height));

		printf("This is this experiment: %p, and framebuffer size: %d, %d \n", this, this->width, height);

}

GLFW_window::GLFW_window(int width_, int height_, const char * window_name, GLFWwindow * * window_in){


	//making window global
	this -> window = window_in;
	//creating the window
	monitor =  glfwGetPrimaryMonitor();
	mode = glfwGetVideoMode(monitor);

	* window = glfwCreateWindow( width_, height_, window_name, NULL, NULL);
	if(!(*(this->window))){
		printf("smth went wrong with winodwInit");
	}


	//getting the size of the framebuffer
	glfwGetFramebufferSize(*(this -> window), &(this->width), &(this->height));

	printf("This is this experiment: %p, and framebuffer size: %d, %d \n", this, this->width, height);

}

GLFW_window::GLFW_window(const char * window_name, GLFWwindow * * window_in){

	//making window "global"
	this -> window = window_in;

	//creating full screen window
	monitor =  glfwGetPrimaryMonitor();
	mode = glfwGetVideoMode(monitor);

	glfwWindowHint(GLFW_RED_BITS, mode->redBits);
	glfwWindowHint(GLFW_GREEN_BITS, mode->greenBits);
	glfwWindowHint(GLFW_BLUE_BITS, mode->blueBits);
	glfwWindowHint(GLFW_REFRESH_RATE, mode->refreshRate);

	*window = glfwCreateWindow(mode->width, mode->height, window_name, monitor,NULL);


	//getting the size of the framebuffer
	glfwGetFramebufferSize(*(this -> window), &(this->width), &(this->height));
	printf("This is this experiment: %p, and framebuffer size: %d, %d \n", this, this->width, height);

}

GLFW_window::~GLFW_window(){
	printf("window is being deleted! \n");
}









void GLFW_window::update(){

	mode = glfwGetVideoMode(monitor);
	glfwGetFramebufferSize(*(this -> window), &(this->width), &(this->height));

}



void GLFW_window::enable_depth(){
	glEnable(GL_DEPTH_TEST);
	depth_enabled = true;
}


void GLFW_window::use(){

	glfwMakeContextCurrent(*(this->window));
	glViewport(0, 0, this->width, this->height);

}


void GLFW_window::set_BG(float r, float g, float b){

	glClearColor(r, g, b, 1.0f);
	glClear(GL_COLOR_BUFFER_BIT);

}

void GLFW_window::set_BGD(float r, float g, float b){

	glClearColor(r, g, b, 1.0f);
	if (depth_enabled){
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	}
	else 	glClear(GL_COLOR_BUFFER_BIT);


}

void GLFW_window::draw(){

	glfwSwapBuffers(*(this -> window));

}





/*********************************************************************************************
**********************************************************************************************
**********************************************************************************************
**********************************************************************************************
* shader class
**********************************************************************************************
**********************************************************************************************
**********************************************************************************************
**********************************************************************************************/

shader_program::shader_program(const char * vertexshader_path, const char * fragmentshader_path){


	//init for the shaders
	GLuint vertexshader, fragmentshader;

	vertexshader = glCreateShader(GL_VERTEX_SHADER);
	fragmentshader = glCreateShader(GL_FRAGMENT_SHADER);



	/*********************************************************************************************
	* setting up shader program
	**********************************************************************************************/
	const GLchar * vshader;
	const GLchar * fshader;

	GLchar * temp_vshader = (GLchar *)malloc(1024);
	GLchar * temp_fshader = (GLchar *)malloc(1024);

	int i = 0;

	std::ifstream file1;
	file1.open(vertexshader_path, std::ios::in);
	if(!file1) {printf("Error with compiling \n"); exit(0);}


	while (file1.good())
    {
       temp_vshader[i] = (GLchar)file1.get();
       if (!file1.eof())
        i++;
    }

    temp_vshader[i] = 0;  // 0-terminate it at the correct position
    file1.close();

    std::ifstream file2;
	file2.open(fragmentshader_path, std::ios::in);
	if(!file2) {std::cout<<"ERROR!2"; exit(0);}

	i =0;

	while (file2.good())
    {
       temp_fshader[i] = (GLchar)file2.get();
       if (!file2.eof())
        i++;
    }

    temp_fshader[i] = 0;  // 0-terminate it at the correct position
    file2.close();

    vshader = temp_vshader;
    fshader = temp_fshader;

	/*********************************************************************************************
	* Compiling shader program
	**********************************************************************************************/

	glShaderSource(vertexshader, 1, &vshader, NULL);
	glCompileShader(vertexshader);

	GLint status;

	glGetShaderiv(vertexshader, GL_COMPILE_STATUS, &status);

	if (status != GL_TRUE){
		char logBuf[1024];
    	glGetShaderInfoLog( vertexshader, sizeof(logBuf), NULL, (GLchar*)logBuf );
    	std::cout<< logBuf;


		std::cout<< "vertexshader compilation error! \n";
		exit(0);
	}

	glShaderSource(fragmentshader, 1, &fshader, NULL);
	glCompileShader(fragmentshader);



	glGetShaderiv(fragmentshader, GL_COMPILE_STATUS, &status);


	if (status != GL_TRUE){

		char logBuf[1024];
    	glGetShaderInfoLog( fragmentshader, sizeof(logBuf), NULL, (GLchar*)logBuf );
    	std::cout<< logBuf;

		std::cout<< "fragmentshader compilation error \n";
		exit(0);
	}

	/*********************************************************************************************
	* Initialising shader program and deleting stuff
	**********************************************************************************************/

	this -> shaderprogram = glCreateProgram();
	glAttachShader(this -> shaderprogram, vertexshader);
	glAttachShader(this -> shaderprogram, fragmentshader);
	glLinkProgram(this -> shaderprogram);


	glGetShaderiv(fragmentshader, GL_LINK_STATUS, &status);



	if (status != GL_TRUE){

		char logBuf[1024];
    	glGetShaderInfoLog( fragmentshader, sizeof(logBuf), NULL, (GLchar*)logBuf );
    	std::cout<< logBuf;

		std::cout<< "linking error!\n";
		exit(0);
	}



	glDeleteShader(vertexshader);
	glDeleteShader(fragmentshader);
	free(temp_vshader);
	free(temp_fshader);

}





shader_program::~shader_program(){
	printf("shader_program is being deleted \n");
}


void shader_program::use(){

	glUseProgram(shaderprogram);

}





/*********************************************************************************************
**********************************************************************************************
* Key reactions
**********************************************************************************************
**********************************************************************************************/


void key_callback(GLFWwindow* window, int key, int scancode, int action, int mods){
    if (key == GLFW_KEY_ESCAPE && action == GLFW_PRESS)
        glfwSetWindowShouldClose(window, GL_TRUE);
}



/*********************************************************************************************
**********************************************************************************************
* drawing
**********************************************************************************************
**********************************************************************************************/

void offset_calc(float * offset, float offset_delta, bool change){
	if(change){
		while( (*(offset+1)) > -0.75 && (*(offset)) <0.01) {
			*(offset)	 += 0;
			*(offset+1)	 += (-1*offset_delta);
		}
	}

}

void offset_calc_circle(float * offset, double * theta, bool change){

	if (change){
		(*theta) += 0.015;
		(*offset) = cos(*theta)/2;
		(*(offset+1)) = sin(*theta)/2;
	}
	else{
		(*theta) -= 0.025;
		(*offset) = cos(*theta)/2;
		(*(offset+1)) = sin(*theta)/2;
	}
}
